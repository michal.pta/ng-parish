import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './services/auth-guard.service';
import { ParishService } from './services/parish.service';
import { AlertService } from './services/alert.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { DataTableModule, AutoCompleteModule, GrowlModule, MessagesModule, ProgressSpinnerModule,
  CalendarModule, InputSwitchModule, ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';

import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ParishionerComponent } from './components/parishioner/parishioner.component';
import { ParishionerBoxComponent } from './components/parishioner-box/parishioner-box.component';
import { ReportsComponent } from './components/reports/reports.component';
import { UsersComponent } from './components/users/users.component';

import { environment } from '../environments/environment';
import { SumOfOfferingsReportComponent } from './components/reports/sum-of-offerings-report/sum-of-offerings-report.component';
import { CatalogueReportComponent } from './components/reports/catalogue-report/catalogue-report.component';
import { ParishionerMergeWizardComponent } from './components/parishioner-merge-wizard/parishioner-merge-wizard.component';
import { QuickAddComponent } from './components/quick-add/quick-add.component';
import { OfferingFormComponent } from './components/offering-form/offering-form.component';
import { ParishionerFormComponent } from './components/parishioner-form/parishioner-form.component';
import { VoiceRecognitionService } from './services/voice-recognition.service';
import { VoiceRecorderComponent } from './components/voice-recorder/voice-recorder.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard], pathMatch: 'full' },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard], pathMatch: 'full' },
  { path: 'parishioners/:id', component: ParishionerComponent, canActivate: [AuthGuard], pathMatch: 'full' },
  { path: 'parish', component: HomeComponent, canActivate: [AuthGuard], pathMatch: 'full' },
  { path: '', component: HomeComponent, canActivate: [AuthGuard], pathMatch: 'full' }
];

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

export function highchartsFactory() {
  const hc = require('highcharts');
  return hc;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ParishionerComponent,
    ParishionerBoxComponent,
    ReportsComponent,
    UsersComponent,
    SumOfOfferingsReportComponent,
    CatalogueReportComponent,
    ParishionerMergeWizardComponent,
    QuickAddComponent,
    OfferingFormComponent,
    ParishionerFormComponent,
    VoiceRecorderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule, AngularFireAuthModule,
    DataTableModule, AutoCompleteModule, GrowlModule,  ProgressSpinnerModule,
    MessagesModule, CalendarModule, InputSwitchModule, ConfirmDialogModule,
    ChartModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  providers: [
    AuthGuard,
    ParishService,
    AlertService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    },
    ConfirmationService,
    VoiceRecognitionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
