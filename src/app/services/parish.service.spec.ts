/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ParishService } from './parish.service';

describe('ParishService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParishService]
    });
  });

  it('should ...', inject([ParishService], (service: ParishService) => {
    expect(service).toBeTruthy();
  }));
});
