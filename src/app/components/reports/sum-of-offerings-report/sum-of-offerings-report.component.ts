import { Component, OnInit, Input } from '@angular/core';
import { ParishService } from 'app/services/parish.service';
import { environment } from 'environments/environment';
import { Parishioner } from 'app/models/parishioner';

@Component({
  selector: 'app-sum-of-offerings-report',
  templateUrl: './sum-of-offerings-report.component.html',
  styleUrls: ['./sum-of-offerings-report.component.css']
})
export class SumOfOfferingsReportComponent implements OnInit {

  @Input() dateStart: Date;
  @Input() dateEnd: Date;

  parishioners: Parishioner[];
  offerers: any[];
  sum: number;

  constructor(private parishService: ParishService) { }

  ngOnInit() {
    this.getReport();
  }

  private getReport() {
    this.parishService.getParishioners()
      .first()
      .do(p => this.parishioners = p)
      .flatMap(() => this.parishService.getOfferingsFromDateRange(this.dateStart, this.dateEnd))
      .first()
      .do(o => this.sum = o.reduce((sum, v) => sum + parseFloat(v.amount), 0))
      .map(o => o.reduce<{parishioner: Parishioner, sum: number}[]>((parishioners, v) => {
        const offeringParishioner = this.parishioners.find(p => p.$key === v.parishioner);
        let parishioner = parishioners.find(e => e.parishioner === offeringParishioner);
        if (!parishioner) {
          parishioner = { parishioner: offeringParishioner, sum: parseFloat(v.amount) }
          parishioners.push(parishioner);
        } else {
          parishioner.sum += parseFloat(v.amount);
        }
        return parishioners;
      }, []))
      .map(p => p.sort((a, b) => b.parishioner.name < a.parishioner.name ? 1 : -1))
      .do(p => this.offerers = this.getOfferersByCity(p))
      .do(p => console.log(this.offerers))
      .subscribe();
  }

  private getOfferersByCity(parishioners: {parishioner: Parishioner, sum: number}[])
    : {city: string, offerers: {parishioner: Parishioner, sum: number}[]}[] {
    const cities = environment.reportConfig.cities;
    const otherCities = environment.reportConfig.otherCities;

    const offerers = []

    cities.forEach(c => offerers.push({
        city: c,
        offerers: parishioners.filter(p => p.parishioner.city === c)
      }));

    offerers.push({
        city: otherCities,
        offerers: parishioners.filter(p => cities.every(c => c !== p.parishioner.city))
      });

    return offerers;
  }
}
