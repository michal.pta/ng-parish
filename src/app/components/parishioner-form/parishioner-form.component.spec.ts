import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParishionerFormComponent } from './parishioner-form.component';

describe('ParishionerFormComponent', () => {
  let component: ParishionerFormComponent;
  let fixture: ComponentFixture<ParishionerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParishionerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParishionerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
