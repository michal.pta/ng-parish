import { Component, OnInit, ElementRef, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';
import { Subscription } from 'rxjs/Subscription';
import { ParishService } from 'app/services/parish.service';
import { Offering } from 'app/models/offering';
import { Parishioner } from 'app/models/parishioner';
import { TranslateService } from 'ng2-translate';
import { AutoComplete, ConfirmationService } from 'primeng/primeng';
import { AlertService } from 'app/services/alert.service';
import { VoiceRecognitionService } from 'app/services/voice-recognition.service';

@Component({
  selector: 'app-quick-add',
  templateUrl: './quick-add.component.html',
  styleUrls: ['./quick-add.component.css']
})
export class QuickAddComponent implements OnInit, OnDestroy {

  parishionerSuggestions: any[];

  parishioners: Parishioner[];
  parishionersSub: Subscription;

  parishioners$: Observable<Parishioner[]>;
  latestOfferings$: Observable<Offering[]>;

  selectedParishioner: Parishioner = null;
  @Output() selectedParishionerChange: EventEmitter<Parishioner> = new EventEmitter();

  searching = false;

  offering: Offering;

  quickAddMessages: any[] = [];

  newParishioner: Parishioner = new Parishioner();
  addParishionerFormVisible = false;

  @ViewChild('parishionerInput') parishionerInput: AutoComplete;

  voiceSub: Subscription;

  constructor(
    private parishService: ParishService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private alertService: AlertService,
    private voice: VoiceRecognitionService) { }

  ngOnInit() {
    this.parishioners$ = this.parishService.getParishioners();
    this.latestOfferings$ = this.getLatestOfferings();

    this.parishionersSub = this.parishioners$.subscribe(p => this.parishioners = p);

    this.voiceSub = this.voice.getResults()
      .filter(() => !(this.selectedParishioner && this.selectedParishioner.$key))
      .subscribe(result => this.trySelectParishioner(result));

    setTimeout(() => this.parishionerInput.inputEL.nativeElement.select(), 100);
  }

  ngOnDestroy() {
    this.parishionersSub.unsubscribe();
    this.voiceSub.unsubscribe();
  }

  trySelectParishioner(query: string): void {
      this.parishionerInput.focusInput();
      this.parishionerInput.inputEL.nativeElement.value = query;
      this.parishionerInput.inputEL.nativeElement.select();
      this.getParishionerSuggestions({ query });
      if (this.parishionerSuggestions.length == 1) {
        this.selectedParishioner = this.parishionerSuggestions[0];
        this.selectParishioner();
      } 
      else {
        this.parishionerInput.show();
      }
  }

  getLatestOfferings(): Observable<Offering[]> {
    const dateStart = new Date(Date.now())
    dateStart.setDate(dateStart.getDate() - 1);
    const dateEnd = new Date(Date.now())
    dateEnd.setDate(dateEnd.getDate() + 1);

    return this.parishService.getOfferingsFromDateRange(dateStart, dateEnd)
      .map(o => o.reverse().slice(0, 5));
  }

  getParishionerById(id: string) {
    return this.parishioners.find(p => p.$key === id);
  }

  getParishionerSuggestions(event: any) {
    if (!event.query) {
      this.parishionerSuggestions = [];
      return;
    }

    let queryWords = (event.query as string).split(' ');

    this.parishionerSuggestions = this.parishioners
      .filter(p => queryWords.every(w => (p.name && p.name.toLowerCase().indexOf(w.toLowerCase()) !== -1
        || p.address && p.address.toLowerCase().indexOf(w.toLowerCase()) !== -1)));
  }

  selectParishioner() {
    if (this.selectedParishioner && this.selectedParishioner.$key) {
      this.searching = true;
      this.quickAddMessages = [];
      this.parishService.getParishionerOfferings(this.selectedParishioner.$key)
        .first()
        .do(o => {
          this.offering = new Offering();
          this.offering.parishioner = this.selectedParishioner.$key;
          this.offering.amount = (o.length > 0 ? parseFloat(o.pop().amount) : 0).toString();
        })
        .finally(() => {
          this.searching = false;
        })
        .subscribe();
    } else {
      this.offering = undefined;
    }
    this.selectedParishionerChange.emit(this.selectedParishioner);
  }

  selectOffering(offering: Offering) {
    const parishioner = this.getParishionerById(offering.parishioner);
    this.selectedParishioner = parishioner;
    this.selectedParishionerChange.emit(this.selectedParishioner);
    this.offering = { ...offering };
    this.quickAddMessages = [];
  }

  handleAddSuccess() {
    this.quickAddMessages = [{
      severity: 'success',
      summary: this.translate.instant('Success'),
      detail: this.translate.instant('AddOfferingAlert', { name: this.selectedParishioner.name })
    }];
    this.reset();
  }

  handleUpdateSuccess() {
    this.quickAddMessages = [{
      severity: 'success',
      summary: this.translate.instant('Success'),
      detail: this.translate.instant('UpdateOfferingAlert', { name: this.selectedParishioner.name })
    }];
    this.reset();
  }

  deleteOffering(offering: Offering) {
    this.confirmationService.confirm({
      message: this.translate.instant('DeleteConfirmationOffering'),
      accept: () => this.parishService.deleteOffering(offering)
        .then(() => {
          this.alertService.alertTranslated('DeleteOfferingAlert');
          if (this.offering.$key === offering.$key) {
            this.reset();
          }
        })
    })
  }

  reset() {
    this.selectedParishioner = undefined;
    this.selectedParishionerChange.emit(this.selectedParishioner);
    this.offering = undefined;
    setTimeout(() => this.parishionerInput.inputEL.nativeElement.select(), 100);
  }

  listen() {
    this.voice.start();
  }

  handleParishionerAddSuccess(parishionerReference: any) {
    this.alertService.alertTranslated('AddParishionerAlert');
    this.addParishionerFormVisible = false;
    this.newParishioner = new Parishioner();
    this.selectedParishioner = this.getParishionerById(parishionerReference.key);
    this.selectParishioner();
  }

}
