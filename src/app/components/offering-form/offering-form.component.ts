import { Component, OnInit, Input, EventEmitter, Output, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Offering } from 'app/models/offering';
import { ParishService } from 'app/services/parish.service';
import { TranslateService } from 'ng2-translate';
import { Parishioner } from 'app/models/parishioner';
import { VoiceRecognitionService } from 'app/services/voice-recognition.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offering-form',
  templateUrl: './offering-form.component.html',
  styleUrls: ['./offering-form.component.css']
})
export class OfferingFormComponent implements OnInit, OnDestroy {

  @ViewChild('amountInput') amountInput: ElementRef;

  private _offering: Offering;
  @Input() set offering(value: Offering) {
    this._offering = value;
    setTimeout(() => this.amountInput.nativeElement.select(), 100);
  };
  get offering() {
    return this._offering;
  }

  @Output() addSuccess: EventEmitter<void> = new EventEmitter();
  @Output() updateSuccess: EventEmitter<void> = new EventEmitter();
  @Output() cancelClick: EventEmitter<void> = new EventEmitter();

  @Input() hideCancel = false;

  processing = false;

  voiceSub: Subscription;

  constructor(private parishService: ParishService, private voice: VoiceRecognitionService) { }

  ngOnInit() {
    this.voiceSub = this.voice.getResults()
      .filter(result => !isNaN(parseFloat(result)) && isFinite(parseFloat(result)))
      .subscribe(result => { 
        this.offering.amount = result 
        setTimeout(() => this.amountInput.nativeElement.select(), 100);
      });
  }

  ngOnDestroy() {
    this.voiceSub.unsubscribe();
  }

  submitOffering() {
    if (!this.offering.$key) {
      this.addOffering();
    } else {
      this.updateOffering();
    }
  }

  addOffering() {
    if (!this.offering.parishioner || !this.offering.amount) {
      return;
    }
    this.processing = true;
    this.parishService.addOffering(this.offering.parishioner, this.offering.amount)
      .then(x => {
        this.addSuccess.emit();
        this.processing = false;
      });
  }

  updateOffering() {
    if (!this.offering.parishioner || !this.offering.amount) {
      return;
    }
    this.processing = true;
    this.parishService.updateOffering(this.offering)
      .then(x => {
        this.updateSuccess.emit();
        this.processing = false;
      });
  }

  emitCancel() {
    this.cancelClick.emit();
  }

}
