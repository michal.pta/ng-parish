import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthGuard implements CanActivate {

  user: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase, private router: Router) {
    this.user = afAuth.authState;
  }

  canActivate() {
    return this.user
      .first()
      .flatMap(a => a && a.uid ? this.db.object('acl/' + a.uid).valueChanges().map(e => e !== null) : Observable.of(false))
      .do(a => {
        if (a === false) {
          this.router.navigate(['login']);
        }
      });
  }

  login(): Promise<any> {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(() => {
        const currentUser = this.afAuth.auth.currentUser;
        this.db.object('users/' + currentUser.uid)
          .update({ uid: currentUser.uid, name: currentUser.displayName, email: currentUser.email })
          .then(a => this.router.navigate(['/']));
      });
  }

  logout(): Promise<any> {
     return this.afAuth.auth.signOut()
      .then(() => this.router.navigate(['login']));
  }

}
