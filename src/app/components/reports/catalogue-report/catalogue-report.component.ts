import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'environments/environment';
import { ParishService } from 'app/services/parish.service';
import { Parishioner } from 'app/models/parishioner';
import { Offering } from 'app/models/offering';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-catalogue-report',
  templateUrl: './catalogue-report.component.html',
  styleUrls: ['./catalogue-report.component.css']
})
export class CatalogueReportComponent implements OnInit {

  @Input() dateStart: Date;
  @Input() dateEnd: Date;

  parishioners: Parishioner[];
  offerings: Offering[];

  parishionerCards: { parishioner: Parishioner, offerings: Offering[] }[] = [];

  constructor(private parishService: ParishService) { }

  ngOnInit() {
    const parishCities = environment.reportConfig.cities;

    Observable.forkJoin(
      this.parishService.getParishioners()
        .take(1)
        .map(p => p.filter(parishioner => parishCities.some(c => c === parishioner.city)))
        .do(p => p.sort((a, b) => this.sortParishioners(a, b)))
        .do(p => this.parishioners = p),
      this.parishService.getOfferingsFromDateRange(this.dateStart, this.dateEnd)
        .take(1)
        .do(o => this.offerings = o)
    ).do(() => {
      this.parishionerCards = this.parishioners
        .map(p => ({parishioner: p, offerings: this.offerings.filter(o => o.parishioner === p.$key).reverse().slice(0, 60)}));
    })
    .subscribe();
  }

  private sortParishioners(a: any, b: any): number {
    if (a.city !== b.city) {
      if (!a.city) return -1;
      if (!b.city) return 1;
      if (a.city < b.city) {
        return -1
      } else if (a.city > b.city) {
        return 1;
      }
    }

    if (a.street !== b.street) {
      if (!a.street) return -1;
      if (!b.street) return 1;
      if (a.street < b.street) {
        return -1;
      } else if (a.street > b.street) {
        return 1;
      }
    }

    if (a.streetNumber !== b.streetNumber) {
      if (!a.streetNumber) return -1;
      if (!b.streetNumber) return 1;
      if (parseInt(a.streetNumber) < parseInt(b.streetNumber)) {
        return -1;
      } else if (parseInt(a.streetNumber) > parseInt(b.streetNumber)) {
        return 1;
      }
    }

    return 0;
  }

}
