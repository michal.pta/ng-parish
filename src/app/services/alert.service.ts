import { Injectable } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class AlertService {

  alerts: any[] = [];

  constructor(private translate: TranslateService) { }

  alert(msg: string, header = 'Success', severity = 'success') {
    this.alerts = [];
    this.alerts.push({ severity: severity, summary: header, detail: msg });
  }
  alertTranslated(key: string, param: any = null, headerKey = 'Success', headerParam: any = null, severity = 'success') {
    Observable.forkJoin(this.translate.get(key, param), this.translate.get(headerKey, headerParam))
      .subscribe(t => this.alert(t[0], t[1]));
  }

}
