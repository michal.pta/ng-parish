import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Parishioner } from '../../models/parishioner';
import { ParishService } from '../../services/parish.service';

@Component({
  selector: 'app-parishioners',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  parishioners$: Observable<Parishioner[]>;

  selectedParishioner: Parishioner;

  constructor(private parishService: ParishService) {}

  ngOnInit() {
    this.parishioners$ = this.parishService.getParishioners()
      .map(p => p.sort((a, b) => a.name < b.name ? -1 : 1));
  }

}
