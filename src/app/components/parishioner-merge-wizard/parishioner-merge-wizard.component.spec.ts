import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParishionerMergeWizardComponent } from './parishioner-merge-wizard.component';

describe('ParishionerMergeWizardComponent', () => {
  let component: ParishionerMergeWizardComponent;
  let fixture: ComponentFixture<ParishionerMergeWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParishionerMergeWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParishionerMergeWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
