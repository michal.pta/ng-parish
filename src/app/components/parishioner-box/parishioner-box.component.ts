import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import { Parishioner } from '../../models/parishioner';
import { Offering } from '../../models/offering';
import { ParishService } from '../../services/parish.service';
import { AlertService } from '../../services/alert.service';
import { ConfirmationService } from 'primeng/primeng';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-parishioner-box',
  templateUrl: './parishioner-box.component.html',
  styleUrls: ['./parishioner-box.component.css']
})
export class ParishionerBoxComponent implements OnInit, OnDestroy {

  @Input() set id(value: string) {
    this.id$.next(value);
  }
  get id(): string {
    return this.id$.getValue();
  }
  private id$: BehaviorSubject<string> = new BehaviorSubject(null);

  @Input() readOnly = false;

  @Input() miniMode = false;

  @Input() noLiveReload = false;

  parishioner: Parishioner;

  parishionerSubscription: Subscription;

  parishionerOfferings: Offering[];
  parishionerSumOfOfferings: number;
  parishionerOfferingChartOptions: any;

  parishionerFormVisible = false;
  parishionerMergeWizardVisible = false;

  offering: Offering;

  constructor(public parishService: ParishService,
    private router: Router,
    private alertService: AlertService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService) {}

  ngOnInit() {
    let stream = this.id$
      .filter(id => id !== null)
      .switchMap(id =>

        this.parishService.getParishioner(id)
        .do(p => this.parishioner = p)
        .do(() => this.resetOffering())

        .flatMap(() => this.parishService.getParishionerOfferings(id))
        .do(o => this.parishionerSumOfOfferings = this.getSumFromOfferings(o))
        .map(o => this.miniMode ? o.slice(0.36) : o)
        .map(o => o.sort((a, b) => b.date - a.date))
        .do(o => {
            if (!this.miniMode) {
              this.parishionerOfferingChartOptions = this.getChartOptionsFromOfferings(o.slice(0, 36).reverse())
            };
          })
        .do(o => this.parishionerOfferings = o)

      );

    if (this.noLiveReload) {
      stream = stream.first();
    }

    this.parishionerSubscription = stream.subscribe();
  }

  ngOnDestroy() {
    this.parishionerSubscription.unsubscribe();
  }

  selectOffering(offering: Offering) {
    this.offering = { ...offering };
  }
  handleOfferingAddSuccess() {
    this.alertService.alertTranslated('AddOfferingAlert', { name: this.parishioner.name });
    this.resetOffering();
  }
  handleOfferingUpdateSuccess() {
    this.alertService.alertTranslated('UpdateOfferingAlert', { name: this.parishioner.name });
    this.resetOffering();
  }
  resetOffering() {
    const offering = new Offering();
    offering.parishioner = this.parishioner.$key;
    this.offering = offering;
  }

  deleteOffering(offering: Offering) {
    this.confirmationService.confirm({
      message: this.translate.instant('DeleteConfirmationOffering'),
      accept: () => this.parishService.deleteOffering(offering)
        .then(() => {
          this.alertService.alertTranslated('DeleteOfferingAlert');
          if (this.offering.$key === offering.$key) {
            this.resetOffering();
          }
        })
    })
  }

  handleParishionerUpdateSuccess() {
    this.alertService.alertTranslated('UpdateParishionerAlert');
  }

  deleteParishioner(parishioner: Parishioner) {
    this.confirmationService.confirm({
      message: this.translate.instant('DeleteConfirmationParishioner'),
      accept: () => this.parishService.deleteParishioner(parishioner)
        .then(() => {
          this.alertService.alertTranslated('DeleteParishionerAlert');
          this.router.navigate(['/parish']);
        })
    })
  }

  private getSumFromOfferings(offerings: Offering[]) {
    return offerings.reduce((sum, o) => sum + parseFloat(o.amount), 0);
  }
  private getChartOptionsFromOfferings(offerings: Offering[]) {
    return {
        title: { text: null },
        chart: { height: 300, backgroundColor: 'transparent', zoomType: 'x' },
        xAxis: { categories: offerings.map(v => new Date(v.date).toLocaleDateString()) },
        yAxis: { title: { text: null } },
        series: [{ name: 'zł', data: offerings.map(v => parseFloat(v.amount)) }],
        plotOptions: {
          line: {
            animation: true
          }
        }
      };
  }

}
