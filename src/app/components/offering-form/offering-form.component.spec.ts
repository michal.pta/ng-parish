import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferingFormComponent } from './offering-form.component';

describe('OfferingFormComponent', () => {
  let component: OfferingFormComponent;
  let fixture: ComponentFixture<OfferingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
