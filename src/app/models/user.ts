export class User {
    uid: string;
    name: string;
    email: string;
    isInAcl: boolean;
}
