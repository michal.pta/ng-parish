import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SumOfOfferingsReportComponent } from './sum-of-offerings-report.component';

describe('SumOfOfferingsReportComponent', () => {
  let component: SumOfOfferingsReportComponent;
  let fixture: ComponentFixture<SumOfOfferingsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SumOfOfferingsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SumOfOfferingsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
