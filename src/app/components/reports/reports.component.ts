import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  dateStart: Date;
  dateEnd: Date;

  sumOfOfferingsVisible = false;
  catalogueVisible = false;

  title: string;

  constructor() {}

  ngOnInit() {
    let now = new Date(Date.now());
    let dateStart = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let dateEnd = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);    
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
  }

  getSumOfOfferingsReport() {
    this.hideReports();
    this.sumOfOfferingsVisible = true;
  }

  getCatalogueReport() {
    this.hideReports();
    this.catalogueVisible = true;
  }

  hideReports() {
    this.sumOfOfferingsVisible = false;
    this.catalogueVisible = false;
    this.title = null;
  }

  print() {
    window.print();
  }

}
