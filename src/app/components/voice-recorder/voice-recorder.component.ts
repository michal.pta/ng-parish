import { Component, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'app/services/voice-recognition.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-voice-recorder',
  templateUrl: './voice-recorder.component.html',
  styleUrls: ['./voice-recorder.component.css']
})
export class VoiceRecorderComponent implements OnInit {

  isRecording: Observable<boolean>;
  
  constructor(private voice: VoiceRecognitionService) { }

  ngOnInit() {
    this.isRecording = this.voice.getListeningStatus();
  }

  toggle() {
    this.voice.toggle();
  }

}
