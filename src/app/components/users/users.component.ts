import { Component, OnInit, OnDestroy } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';

import { Subscription } from 'rxjs/Subscription';

import { User } from '../../models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

  users: User[];
  acl: any[];

  sub: Subscription;

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {
    this.db.list<User>('users').snapshotChanges()
      .first()
      .map(changes => changes.map(p => ({ $key: p.payload.key, ...p.payload.val() })))
      .map(p => p.sort((a, b) => a.name < b.name ? -1 : 1))
      .subscribe(u => this.users = u);

    this.sub = this.db.list('acl').snapshotChanges()
      .map(changes => changes.map(p => ({ $key: p.payload.key, ...p.payload.val() })))
      .subscribe(acl => {
        this.acl = acl;
        this.users.forEach(user => user.isInAcl = this.isInAcl(user));
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  isInAcl(user: User): boolean {
    return this.acl && this.acl.find(e => e.$key === user.uid) ? true : false;
  }

  registerInAcl(user: User): void {
    if (user.isInAcl) {
      this.db.object('acl/' + user.uid).set(true);
    } else {
      this.db.object('acl/' + user.uid).remove();
    }
  }

}
